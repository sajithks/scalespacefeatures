# Classification of Cell Cultures using Scale Space and Graph Features #

In this project, we evaluate the usability of scale space and graph features for classification of different samples of cancer cells. The main aim of this project is to evaluate the usability of scale space features using graph approach and modify the existing scale space catastrophe histogram .
##Methodology##
The methodology include extraction of scale space features, especially catastrophe points. A delaunay graph is constructed using the catastrophe points various graph based features are extracted. The final classification and evaluation of classifier is done on these graph features.

##Input image and preprocessing details##
Input image consists of $5$ sets of images each set acquired from a specific patient. Each set consists of image acquired using $3$ different wavelengths of light. Out of $3$ we use only one wavelength, which illuminates a specific protien located in cytoplasm regions of cells. Each images are of resolution $2160 \times 2160$ as shown in Fig \ref{fig:inputimg}. In order to increase the dataset size we divide each image into $4$ equal parts of size $1080 \times 1080$.

## Feature extraction##

Feature extraction is done by extracting catastrophe points from the image as explained in \cite{sajith2013}. Once the catastrophe features are extracted, delaunay triangulation \cite{weisstein2016} is constructed. A sample is shown in Fig \ref{fig:delaunay}. After creating delaunay triangulation, the following graph features are extracted, average clustering\cite{latapy2008}, density, estrada index\cite{estrada2000}, mean and maximum value of adjacency spectrum. 
	
	Let $G(V,E)$ be an undirected graph, with $\lambda_1, \lambda_2 ... \lambda_n$ be decreasing eigen values of its adjacency matrix, then
	Estrada index, $e = \sum_{j=1}^{n}e^{\lambda_j}$
	
	To preserve the scale of different features the graph features are calculated separately for maxima-saddle and minima-saddle catastrophes at each scale. So for each scale we extract $5$ features for both type of catastrophes. In this experiment the input is analysed at $20$ scales resulting in a total feature vector length of $200$ for each input image.

##Training and classification ##
The extracted features are preprocessed to zero mean and unit standard deviation. For classification we used an SVM classifier with $10$ fold cross validation. The average score for $10$ cross validations is obtained.

##Installation Details##
The entire processing pipeline is implemented in python with some part of the compute intesive operations for calculating scale space features were ported to C++. For delaunay triangulation Scipy \cite{jones2001} was used. Networkx \cite{aric2008} library was used to extract graph features, and Scikit-learn \cite{pedregosa2011} was used preprocessing and classification. 
To run the program we need to meet the following dependencies in Ubuntu 14.04. 

* Python
* Opencv for python
* Boost, boost-python, boost-system
* Scipy, numpy,scikit-learn, networkx

To build the system the following commands are used.

```$cmake CMakeLists.txt```

```$make```

To run the program run

```$python classify_pipeline_withcrop.py
```
##References##
[1] S. Sajith Kecheril, et al: Automated lung cancer detection by the analysis of glandular cells in sputum cytology images using scale space features. In: Signal, Image and Video Processing, vol.9, issue 4, pp. 851-863, 2013.

[2] Weisstein, Eric W. Delaunay Triangulation. From MathWorld--A Wolfram Web Resource. http://mathworld.wolfram.com/DelaunayTriangulation.html 

[3] E. Estrada, Characterization of 3D molecular structure, Chem. Phys. Lett. 319, 713 , 2000.

[4] Latapy, Matthieu, Cl\'emence Magnien, and Nathalie Del Vecchio, Basic notions for the analysis of large two-mode networks. Social Networks 30(1), 31–48, 2000.

[5] Jones E, Oliphant E, Peterson P, et al. SciPy: Open Source Scientific Tools for Python, 2001-, http://www.scipy.org/ [Online; accessed 2016-04-03].

[6] Aric A. Hagberg, Daniel A. Schult and Pieter J. Swart, Exploring network structure, dynamics, and function using NetworkX, in Proceedings of the 7th Python in Science Conference (SciPy2008), G\"ael Varoquaux, Travis Vaught, and Jarrod Millman (Eds), (Pasadena, CA USA), pp. 11–15, Aug 2008

[7] Pedregosa et al. Scikit-learn: Machine Learning in Python, JMLR 12, pp. 2825-2830, 2011.
		
