# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 14:16:59 2016

@author: sajith
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:39:11 2016

@author: Sajith

Full pipe line to find catastrophe points, create graph, extract features, classify and cross validate.
"""

import os
import sys

sys.path.append('/home/sajith/scalespace')

import cv2
import numpy as np
#import vtk
from scipy.spatial import Delaunay
import ssfeatures
import networkx as nx
import time
#import ss
import time
import pickle
import glob
import string

from sklearn import cross_validation
#from sklearn import datasets
from sklearn import svm
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
import logging
#%%

def findCatastrophePoints(inputgray):
    if(np.shape(inputgray.shape)[0]==3):    
        inputgray = inputgray[:,:,0]
        
    initsigma = 02.00
    finalsigma = 3.0
    levels = 20
    cat = ssfeatures.findCatastrophe(inputgray, initsigma, finalsigma, levels)
    return(cat)

#%%

inputimagefolder = '/media/sf_vmshare/Ida_dataset/'
imagefolderlist = sorted(glob.glob(inputimagefolder+'*'))

experimentfolderbase = '/home/sajith/scalespace/'
experimentname = 'experiment1' # mandatory to give a name to experiment

if(os.system('mkdir '+experimentfolderbase+experimentname) >0):
    raise ValueError('Folder already exists!!!')

experimentbase = experimentfolderbase+experimentname

LOG_FILENAME = experimentbase +'/'+ 'logfile.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
logging.debug('This message should go to the log file')


os.system('mkdir '+experimentbase+'/catastrophepoints')

catfolderbase = experimentbase+'/catastrophepoints'

os.system('mkdir '+experimentbase+'/graphfeatures')

featfolderbase = experimentbase+'/graphfeatures'

#%% Read image extract catastrophe points 
print " Reading images and extracting catastrophe points..."
#loop through each folder
for singlefolder in imagefolderlist:
    currfoldername = string.split(singlefolder,'/')[-1]
    os.system('mkdir '+catfolderbase+'/'+currfoldername)
    currfolderbase = catfolderbase+'/'+currfoldername
#    read all file names in this folder
    allimagenamelist = glob.glob(singlefolder+'/'+'*w2.TIF') # only wavelength 2 considered now
    
    for singlefile in allimagenamelist:
        currfilename = string.split( string.split(singlefile,'/')[-1], '.')[0]+'.p'
        inputgray = cv2.imread(singlefile,-1)
        catfeatures = findCatastrophePoints(inputgray)
        pickle.dump(catfeatures, open(currfolderbase+'/'+currfilename,'wb'))
        

print " saved catastrophe points..."
    
#%% Read catastrophe points and extract graph features

#catfolderbase = '/media/sf_vmshare/features'

print "Graph feature extraction going on..."

featfolderlist = sorted(glob.glob(catfolderbase+'/'+'*'))

featsize = np.arange(10,100,10)
for fsize in featsize:
        for singlefolder in featfolderlist:
            allfeat =[]
            currfoldername = string.split(singlefolder,'/')[-1]
        #    os.system('mkdir '+featfolderbase+'/'+currfoldername)
        #    currfolderbase = featfolderbase+'/'+currfoldername
        #    read all file names in this folder
            allimagenamelist = glob.glob(singlefolder+'/'+'*p') # only wavelength 2 considered now
            
            for singlefile in allimagenamelist:
                currfilename = string.split( string.split(singlefile,'/')[-1], '.')[0]+'.p'
                catfeatures = pickle.load(open(singlefile,'rb'))
                
                adjmat = np.zeros((catfeatures.shape[0], catfeatures.shape[0]))    
            
                tri = Delaunay(np.array([catfeatures[:, 0], catfeatures[:, 1], catfeatures[:, 2]]).T)
                
                dist = np.zeros((0))
                for singlesimp in tri.simplices:
            #        adjmat[cc[0], cc[1:4]] = 1
                    dist = np.hstack((dist, np.sqrt(np.sum((catfeatures[singlesimp[0],0:3]-catfeatures[singlesimp[1:4],0:3])**2,1)) )) # euclidean distance between points
        #            adjmat[singlesimp[0], singlesimp[1:4]] = np.sqrt(np.sum((catfeatures[singlesimp[0],0:3]-catfeatures[singlesimp[1:4],0:3])**2,1)) # euclidean distance between points
        #        feat = np.histogram(dist,10)[0]
                feat = np.histogram(dist,fsize)[0]
        
                
                #% convert to networkx graph#%% extract graph features
            
        #        catgraph = nx.Graph(adjmat)
         
        #        f1 =  nx.algorithms.average_clustering(catgraph)
        
        
        #        hf1 = np.histogram(nx.laplacian_spectrum(catgraph), 10)[0]*1.
            #hf1 = hf1/hf1.max()
            
        #        hf2 = np.histogram(np.real(nx.adjacency_spectrum(catgraph)), 10)[0]*1.
            #hf2 = hf2/hf2.max()
            
        #        hf3 = np.histogram(nx.average_neighbor_degree(catgraph).values(), 10)[0]*1.
            #hf3 = hf3/hf3.max()
            
        #        feat = np.hstack((hf1, hf2, hf3))
            
                allfeat.append(feat)
        
            pickle.dump(np.array(allfeat),open(featfolderbase+'/'+currfoldername+'.p','wb')) 
        
        print "Graph features saved..."   
        #% classification and scoring
        
        print "classifying..."
        graphfeatfolderlist = glob.glob(featfolderbase+'/'+'*' )
        #
        featshape = feat.shape    
        featurevec = np.zeros((0,featshape[0]))
        #featurevec = np.zeros((0,10))
        label = np.zeros((0))
        classcount = 0
        
        for singleclass in graphfeatfolderlist:
        #    start = time.time()
        #    print pickle.load(open(ff,'rb')).shape
        #    print 'file ', ff, ' processing... '
            singleclassfeat = pickle.load(open(singleclass,'rb'))
            featurevec = np.vstack( ( featurevec, singleclassfeat ))
            label = np.hstack((label, (np.ones((singleclassfeat.shape[0]))*classcount).T ))
            
            classcount = classcount + 1
        
        #% classifier
        clf = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=1, random_state=0)
        scaler = preprocessing.StandardScaler().fit(featurevec)
#        featurevec_transformed = featurevec
        featurevec_transformed = scaler.transform(featurevec)
        
        #clf = svm.SVC(kernel='poly', C=1, gamma=0.1000)
        scores = cross_validation.cross_val_score(clf, featurevec_transformed, label, cv=10)
        #scores = cross_validation.cross_val_score(clf, c, l, cv=5)
        
        print scores.mean()
