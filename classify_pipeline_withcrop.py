# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 18:40:25 2016

@author: sajith
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 14:16:59 2016

@author: sajith
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:39:11 2016

@author: Sajith

Full pipe line to find catastrophe points, create graph, extract features, classify and cross validate.
"""

import os
import sys

sys.path.append('/home/sajith/scalespace')

import cv2
import numpy as np
#import vtk
from scipy.spatial import Delaunay
import ssfeatures
import networkx as nx
import time
#import ss
import time
import pickle
import glob
import string

from sklearn import cross_validation
#from sklearn import datasets
from sklearn import svm
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
#from sklearn.feature_selection import SelectFromModel


import logging
#%%

def findCatastrophePoints(inputgray):
    if(np.shape(inputgray.shape)[0]==3):    
        inputgray = inputgray[:,:,0]
        
    initsigma = 01.00
    finalsigma = 3.0
    levels = 20
    cat = ssfeatures.findCatastrophe(inputgray, initsigma, finalsigma, levels)
    return(cat)

#%%
starttime = time.time()

inputimagefolder = '/media/sf_vmshare/Ida_dataset/'
imagefolderlist = sorted(glob.glob(inputimagefolder+'*'))

experimentfolderbase = '/media/sf_vmshare/'
experimentname = 'experiment3' # mandatory to give a name to experiment
experimentbase = experimentfolderbase+experimentname

LOG_FILENAME = experimentfolderbase + experimentname+'_logfile.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
logging.debug('logging starts')

catfolderbase = experimentbase+'/catastrophepoints'
featfolderbase = experimentbase+'/graphfeatures'

newexperiment = 0
getcatfeat = 0
getgraphfeat = 0
classifyall = 1
logging.debug('newexperiment= '+np.str(newexperiment))

if(newexperiment ==1 ):
    if(os.system('mkdir '+experimentfolderbase+experimentname) >0):
        raise ValueError('Folder already exists!!!')
    os.system('mkdir '+experimentbase+'/catastrophepoints')
    os.system('mkdir '+experimentbase+'/graphfeatures')



#%% Read image extract catastrophe points 
if(newexperiment ==1 and getcatfeat==1):
    print " Reading images and extracting catastrophe points..."
    #loop through each folder
    for singlefolder in imagefolderlist:
        currfoldername = string.split(singlefolder,'/')[-1]
        os.system('mkdir '+catfolderbase+'/'+currfoldername)
        currfolderbase = catfolderbase+'/'+currfoldername
    #    read all file names in this folder
        allimagenamelist = glob.glob(singlefolder+'/'+'*w2.TIF') # only wavelength 2 considered now
        
        for singlefile in allimagenamelist:
            currfilename = string.split( string.split(singlefile,'/')[-1], '.')[0]
            inputgray = cv2.imread(singlefile,-1)
            inshape = inputgray.shape
            for row in np.arange(0,inshape[0],inshape[0]/2): # crop image to ease processing and get more feature vectors
                for col in np.arange(0,inshape[1],inshape[1]/2):
                    cropimg = inputgray[row:row+inshape[0]/2, col:col+inshape[1]/2,]
                    
                    catfeatures = findCatastrophePoints(cropimg)
                    outname = currfilename+np.str(row)+'_'+np.str(col)+'.p'
                    pickle.dump(catfeatures, open(currfolderbase+'/'+outname,'wb'))
            
    
    print " saved catastrophe points..."
    
#%% Read catastrophe points and extract graph features

#catfolderbase = '/media/sf_vmshare/features'
if(getgraphfeat==1):

    print "Graph feature extraction going on..."
    
    featfolderlist = sorted(glob.glob(catfolderbase+'/'+'*'))
    
    featsize = np.arange(1,6,1)
#    fsize = 5
#    for fsize in featsize:
    for singlefolder in featfolderlist:
#        allfeat = np.zeros((0))
        featvec = np.zeros((0,200))
        
        currfoldername = string.split(singlefolder,'/')[-1]
    #    os.system('mkdir '+featfolderbase+'/'+currfoldername)
    #    currfolderbase = featfolderbase+'/'+currfoldername
    #    read all file names in this folder
        allimagenamelist = glob.glob(singlefolder+'/'+'*p') # only wavelength 2 considered now
        cc=0
        for singlefile in allimagenamelist:
#            print cc
            cc+=1
            currfilename = string.split( string.split(singlefile,'/')[-1], '.')[0]+'.p'
            catfeatures = pickle.load(open(singlefile,'rb'))
            
            allscale = np.arange(0,20,1)
#            featvec = np.zeros((0,200))
            feat = np.zeros((0))
            
            allfeat = np.zeros((0))
            sumfeat = np.zeros((0))
            
            logging.debug('distance histogram features')
 
            for singscale in allscale:
#                print singscale
                cattype =[1,3]
                for cat_type in cattype:
                    skip = 0
                    singlcatfeatures = catfeatures[catfeatures[:,2]==singscale]
                    singlcatfeatures = singlcatfeatures[singlcatfeatures[:,5]==cat_type]
                    
#                    sumfeat = np.hstack((sumfeat, singlcatfeatures.shape[0]))                   
                    
                    adjmat = np.zeros((singlcatfeatures.shape[0], singlcatfeatures.shape[0]))    
                    try:
                        
                        tri = Delaunay(np.array([singlcatfeatures[:, 0], singlcatfeatures[:, 1]]).T)
    
                    except:
                        feat = np.zeros((5))
                        skip = 1
                    
                    if(skip==0):
                        dist = np.zeros((0))
                        for singlesimp in tri.simplices:
                    #        adjmat[cc[0], cc[1:4]] = 1
#                                dist = np.hstack((dist, np.sqrt(np.sum((singlcatfeatures[singlesimp[0],0:3]-singlcatfeatures[singlesimp[1:4],0:3])**2,1)) )) # euclidean distance between points
                            adjmat[singlesimp[0], singlesimp[1:4]] = np.sqrt(np.sum((singlcatfeatures[singlesimp[0],0:3]-singlcatfeatures[singlesimp[1:4],0:3])**2,1)) # euclidean distance between points
                        catgraph = nx.Graph(adjmat)
                 
                        f1 = nx.algorithms.average_clustering(catgraph)
                        f2 = nx.density(catgraph)
                        f3 = nx.estrada_index(catgraph)                    
#                            hf1 = np.histogram(nx.laplacian_spectrum(catgraph), 10)[0]*1.
                    #hf1 = hf1/hf1.max()
                    
                        f4 = (np.real(nx.adjacency_spectrum(catgraph))).mean()
                        f5 = (np.real(nx.adjacency_spectrum(catgraph))).max()

                        feat = np.hstack((f1, f2, f3, f4, f5))
#                        print feat.shape
#                    feat = np.hstack((f1, hf1, hf2, hf3))
                    
                    allfeat = np.hstack((allfeat,feat))

#            print allfeat.shape
            featvec = np.vstack((featvec,allfeat))
            print featvec.shape
            
        pickle.dump(featvec,open(featfolderbase+'/'+currfoldername+'.p','wb')) 


                #        feat = np.histogram(dist,10)[0]
#%%                            feat = np.histogram(dist,fsize)[0]
                    
#                print featvec.shape
#                print "skip ", skip
                
                            #% convert to networkx graph#%% extract graph features
                        
                            
                        #hf2 = hf2/hf2.max()
                        
#                            hf3 = np.histogram(nx.average_neighbor_degree(catgraph).values(), 10)[0]*1.
                        #hf3 = hf3/hf3.max()
        
#                    featvec = np.hstack((featvec,feat))

    
        
        print "Graph features saved..."   
#%%        
if(classifyall==1):
    print "classifying..."
    graphfeatfolderlist = glob.glob(featfolderbase+'/'+'*' )
    #
#    featshape = featvec.shape    
#    featurevec = np.zeros((0,featshape[1]))
    tempfeat = pickle.load(open(graphfeatfolderlist[0],'rb'))

    featurevec = np.zeros((0,tempfeat.shape[1]))

    label = np.zeros((0))
    classcount = 0
    
    for singleclass in graphfeatfolderlist:
    #    start = time.time()
    #    print pickle.load(open(ff,'rb')).shape
    #    print 'file ', ff, ' processing... '
        singleclassfeat = pickle.load(open(singleclass,'rb'))
        print singleclassfeat.shape
        featurevec = np.vstack( ( featurevec, singleclassfeat ))
        label = np.hstack((label, (np.ones((singleclassfeat.shape[0]))*classcount).T ))
        
        classcount = classcount + 1
    
    #% classifier
#    clf = RandomForestClassifier(n_estimators=200, max_features="sqrt", max_depth=None, min_samples_split=20, random_state=0)
#    featurevec_transformed = featurevec
    
#    featurevec_selected = SelectKBest(chi2, k=100).fit_transform(featurevec, label)    
    scaler = preprocessing.StandardScaler().fit(featurevec)

    featurevec_transformed = scaler.transform(featurevec)
    
    clf = svm.SVC(kernel='rbf', C=1, gamma=0.001500)
#    model = SelectFromModel(clf, prefit=True)
#    featurevec_selected = model.transform(featurevec_transformed)
    
    scores = cross_validation.cross_val_score(clf, featurevec_transformed, label, cv=10)
    #scores = cross_validation.cross_val_score(clf, c, l, cv=5)
    
    print scores.mean()
    logging.debug('scores')

    logging.debug(np.str(scores.mean()))

print "total time taken: ", time.time()-starttime