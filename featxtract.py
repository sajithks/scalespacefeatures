# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 14:05:14 2016

@author: sajith
"""

import sys
import os
sys.path.append('/home/sajith/scalespace')

import cv2
import sys
import numpy as np
#import vtk
from scipy.spatial import Delaunay
import ssfeatures
import networkx as nx
import time
import ss
import time
import pickle
import glob
import string
#%%

infolder = '/home/sajith/scalespace/features/3245P01controls/'
filelist = sorted(glob.glob(infolder+'*.p'))
outfolder = '/media/sf_vmshare/3245P01controls/'
allfeat = []
#filename = '/home/sajith/scalespace/phasedata3.tif'
print np.shape(filelist)
for ff in filelist:
    print 'file ', ff, ' processing... '
    cat = pickle.load(open(ff,'rb'))
#%% catastrophe to delaunay graph
    adjmat = np.zeros((cat.shape[0], cat.shape[0]))    
    
    tri = Delaunay(np.array([cat[:, 0], cat[:, 1], cat[:, 2]]).T)

    for cc in tri.simplices:
        adjmat[cc[0], cc[1:4]] = 1
    
    
    #%% convert to networkx graph

    catgraph = nx.Graph(adjmat)
    
    # create nodes
#    for ii in xrange(cat.shape[0]):
#        catgraph.add_node(ii, pos = [cat[ii,0],cat[ii,1],(cat[ii,2]+0.0)*1], extr_hess =cat[ii,3], saddl_hess =cat[ii, 4], cat_type =cat[ii, 5]  )
    
#    lenvec = []
#    for ii in tri.simplices:
#        for jj in np.arange(0, ii.shape[0]-1):
#            for kk in np.arange(jj+1,ii.shape[0]):
#                lenval = np.linalg.norm(cat[ii[jj],0:2]-cat[ii[kk],0:2])
#                lenvec.append(lenval)            
#                catgraph.add_edge(ii[jj], ii[kk], length= lenval)
#    
    
    #%% graph features
    #f1 = nx.adamic_adar_index(catgraph)
    #f1 = nx.algebraic_connectivity(catgraph)
    #f3 = nx.average_node_connectivity(catgraph)
    #f2 = nx.average_shortest_path_length(catgraph)
    
    hf1 = np.histogram(nx.laplacian_spectrum(catgraph), 10)[0]*1.
    #hf1 = hf1/hf1.max()
    
    hf2 = np.histogram(np.real(nx.adjacency_spectrum(catgraph)), 10)[0]*1.
    #hf2 = hf2/hf2.max()
    
    hf3 = np.histogram(nx.average_neighbor_degree(catgraph).values(), 10)[0]*1.
    #hf3 = hf3/hf3.max()
    
    feat = np.hstack((hf1, hf2, hf3))
    
    allfeat.append(feat)
