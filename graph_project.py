"""
@author: Sajith Kecheril Sadanandan
sajith.ks@it.uu.se
"""
import sys
import os
#sys.path.append('/Users/sajithks/Documents/Courses/2015/Graph_image_processing/project')

import cv2
import sys
import numpy as np
#import vtk
from scipy.spatial import Delaunay
import ssfeatures
import networkx as nx
import time
import ss
import time
import pickle
import glob
import string
#%%


#%% reading input image and finding scale space features such as critical points and catastrosphes
#filename = sys.argv[1]
#filename = '/Users/sajithks/Documents/Courses/2015/Graph_image_processing/project/quartcushion1-a-p001.jpg'
infolder = '/media/sf_vmshare/3182P01controls/'
filelist = sorted(glob.glob(infolder+'*w2.TIF'))
outfolder = '/home/sajith/scalespace/features/3182P01controls/'
#filename = '/home/sajith/scalespace/phasedata3.tif'
print np.shape(filelist)
for ii in filelist:
    print 'file ', ii, ' processing... '
    inputgray = cv2.imread(ii,-1)
    #inputgray = cv2.imread(filename,-1)
    
    #start = time.time()
    
    if(np.shape(inputgray.shape)[0]==3):    
        inputgray = inputgray[:,:,0]
    #inputgray = inputgray[0:512,0:512]
    initsigma = 02.00
    finalsigma = 3.0
    levels = 10
    
    #maxima = ss.findMaxima(img )
    #minima = ss.findMinima(img )
    #saddle = ss.findSaddle(img )
    
    #print time.time()-start
    #, minima, saddle 
    
    #starttime = time.time()
    cat = ssfeatures.findCatastrophe(inputgray, initsigma, finalsigma, levels)
    
    outname =  string.split(string.split(ii,'/')[-1], '.' )[0]+'.p'
    pickle.dump(cat, open(outfolder+outname,'wb'))


#print time.time()-start

'''
#scaleval = np.linspace(initsigma, finalsigma, levels)

#maximapts = np.zeros((0,2))
minimapts = np.zeros((0,2))
saddlepts = np.zeros((0,2))


#for ii in range(levels):
#    (ma, mi, sa, maxval, minval, saddleval) = ssfeatures.findCriticalPoints(inputgray, np.exp(scaleval[ii]))
#
#    for jj in ma:
#        critpoints.InsertNextPoint(np.hstack((jj, (ii+0.0 )*1)))
#        crittype.InsertNextValue(3)
#
#    for jj in mi:
#        critpoints.InsertNextPoint(np.hstack((jj, (ii+0.0 )*1)))
#        crittype.InsertNextValue(1)
#        
#    for jj in sa:
#        critpoints.InsertNextPoint(np.hstack((jj, (ii+0.0 )*1)))
#        crittype.InsertNextValue(2)
#
#critdata.SetPoints(critpoints)
#critdata.GetPointData().SetScalars(crittype)



#%% catastrophe to delaunay graph

tri = Delaunay(np.array([cat[:, 0], cat[:, 1], cat[:, 2]]).T)


#%% convert to networkx graph

catgraph = nx.Graph()

# create nodes
for ii in xrange(cat.shape[0]):
    catgraph.add_node(ii, pos = [cat[ii,0],cat[ii,1],(cat[ii,2]+0.0)*1], extr_hess =cat[ii,3], saddl_hess =cat[ii, 4], cat_type =cat[ii, 5]  )

lenvec = []
for ii in tri.simplices:
    for jj in np.arange(0, ii.shape[0]-1):
        for kk in np.arange(jj+1,ii.shape[0]):
            lenval = np.linalg.norm(cat[ii[jj],0:2]-cat[ii[kk],0:2])
            lenvec.append(lenval)            
            catgraph.add_edge(ii[jj], ii[kk], length= lenval)


#%% graph features
#f1 = nx.adamic_adar_index(catgraph)
#f1 = nx.algebraic_connectivity(catgraph)
#f3 = nx.average_node_connectivity(catgraph)
f2 = nx.average_shortest_path_length(catgraph)

hf1 = np.histogram(nx.laplacian_spectrum(catgraph), 10)[0]*1.
#hf1 = hf1/hf1.max()

hf2 = np.histogram(np.real(nx.adjacency_spectrum(catgraph)), 10)[0]*1.
#hf2 = hf2/hf2.max()

hf3 = np.histogram(nx.average_neighbor_degree(catgraph).values(), 10)[0]*1.
#hf3 = hf3/hf3.max()

feat = np.hstack(( f2, hf1, hf2, hf3))

print time.time()-starttime


'''







#unique connections
#citer = catgraph.nodes_iter() # graph iterator
#
#conn = np.zeros((0,2))
#for ii in citer:
#    conn = np.vstack((conn, catgraph.edges(ii)))
#    
#conn = np.sort(conn)
#
#uniqconn = np.unique(conn.view(np.dtype((np.void, conn.dtype.itemsize*conn.shape[1])))).view(conn.dtype).reshape(-1, conn.shape[1])
#
#uniqconn = np.int32(uniqconn)

#%% porting catastrophe points to vtk

#citer = catgraph.nodes_iter() # graph iterator

#catdata = vtk.vtkPolyData()
#
#points = vtk.vtkPoints() # co-ordinates
#cattype = vtk.vtkFloatArray() # maxima(3) or minima(1) catastrophe
#neigh = vtk.vtkCellArray() # neighbours
#
#for ii in citer:
#    points.InsertNextPoint(catgraph.node[ii]['pos'])
#    cattype.InsertNextValue(catgraph.node[ii]['cat_type'])
#        
#
#for ii in uniqconn:
#    neigh.InsertNextCell(2)
#    neigh.InsertCellPoint(ii[0])
#    neigh.InsertCellPoint(ii[1])
#
#catdata.SetPoints(points)
#catdata.GetPointData().SetScalars(cattype)
#catdata.SetLines(neigh)
#
###########################################################################################
##%% visualizing in vtk
#
### reading input image in vtk
#
#pnmReader = vtk.vtkJPEGReader()
#pnmReader.SetFileName(filename)
#pnmReader.SetDataScalarTypeToFloat()
#pnmReader.Update()
#
#imgactor = vtk.vtkImageActor()
#imgactor.GetMapper().SetInputConnection(pnmReader.GetOutputPort())
#
### setting up renderer and interactor
#
#ren1 = vtk.vtkRenderer()
#renWin = vtk.vtkRenderWindow()
#renWin.AddRenderer(ren1)
#iren = vtk.vtkRenderWindowInteractor()
#iren.SetRenderWindow(renWin)
#
################### sphere actor for catastrophe points ###################################
#
#ctfcat = vtk.vtkColorTransferFunction()
#ctfcat.AddRGBPoint(1, 1.0, 0.0, 0.0)
#ctfcat.AddRGBPoint(2, 0.0, 0.0, 1.0)
#ctfcat.AddRGBPoint(3, 0.0, 1.0, 0.0)
#
#catsphere = vtk.vtkSphereSource()
#catsphere.SetRadius(01.00)
#catsphere.SetThetaResolution(8)
#catsphere.SetPhiResolution(8)
#
#catsphereglyph = vtk.vtkGlyph3D()
##ballGlyph.SetInputData(critdata)
#catsphereglyph.SetInputData(catdata)
#catsphereglyph.SetSourceConnection(catsphere.GetOutputPort())
#catsphereglyph.SetScaleModeToScaleByScalar()
#catsphereglyph.SetColorModeToColorByScalar()
#catsphereglyph.SetScaleFactor(1.0)
#catsphereglyph.SetScaleModeToDataScalingOff()
#
#catspheremapper = vtk.vtkPolyDataMapper()
#catspheremapper.SetInputConnection(catsphereglyph.GetOutputPort())
#catspheremapper.SetLookupTable(ctfcat)
#
#catsphereactor = vtk.vtkActor()
#catsphereactor.SetMapper(catspheremapper)
#
################### sphere actor for critical points ###################################
#
#ctfcrit = vtk.vtkColorTransferFunction()
#ctfcrit.AddRGBPoint(1, 0.0, 0.0, 0.0)
#ctfcrit.AddRGBPoint(2, 0.50, 0.50, 0.50)
#ctfcrit.AddRGBPoint(3, 1.0, 1.0, 1.0)
#
#critsphere = vtk.vtkSphereSource()
#critsphere.SetRadius(0.400)
#critsphere.SetThetaResolution(8)
#critsphere.SetPhiResolution(8)
#
#critsphereglyph = vtk.vtkGlyph3D()
#critsphereglyph.SetInputData(critdata)
#critsphereglyph.SetSourceConnection(critsphere.GetOutputPort())
##critsphereglyph.SetScaleModeToScaleByScalar()
##critsphereglyph.SetColorModeToColorByScalar()
#critsphereglyph.SetScaleFactor(1.0)
#critsphereglyph.SetScaleModeToDataScalingOff()
#
#critspheremapper = vtk.vtkPolyDataMapper()
#critspheremapper.SetInputConnection(critsphereglyph.GetOutputPort())
#critspheremapper.SetLookupTable(ctfcrit)
#
#critsphereactor = vtk.vtkActor()
#critsphereactor.SetMapper(critspheremapper)
###
#tubeFilter = vtk.vtkTubeFilter()
#tubeFilter.SetInputData(catdata)
#tubeFilter.SetRadius(0.15)
#tubeFilter.SetNumberOfSides(7)
# 
#tubeMapper = vtk.vtkPolyDataMapper()
#tubeMapper.SetInputConnection(tubeFilter.GetOutputPort())
#
#tubeMapper.ScalarVisibilityOff() 
#
#tubeActor = vtk.vtkActor()
#tubeActor.SetMapper(tubeMapper)
#tubeActor.GetProperty().SetColor(1.0,0.80,0.0)
#
#tubeActor.GetProperty().SetSpecularColor(1, 1, 1)
#tubeActor.GetProperty().SetSpecular(0.3)
#tubeActor.GetProperty().SetSpecularPower(20)
#tubeActor.GetProperty().SetAmbient(0.2)
#tubeActor.GetProperty().SetDiffuse(0.8)
#
#### outline for data
#outlineData = vtk.vtkOutlineFilter()
#outlineData.SetInputConnection(critsphereglyph.GetOutputPort())
#
#outlineMapper = vtk.vtkPolyDataMapper()
#outlineMapper.SetInputConnection(outlineData.GetOutputPort())
#
#outlineActor = vtk.vtkActor()
#outlineActor.SetMapper(outlineMapper)
#outlineActor.GetProperty().SetColor(0, 0, 0)
#outlineActor.SetScale(1.0,1.0,1.0)
#
### default display behaviour
#catsphereactor.SetVisibility(1)
#critsphereactor.SetVisibility(0)
#tubeActor.SetVisibility(0)
#
### legend
#legend = vtk.vtkLegendBoxActor()
#
#legend.UseBackgroundOff()
#legend.GetEntryTextProperty().SetFontSize(6)
##legend.GetEntryTextProperty().SetFontSize(5)
#legend.SetNumberOfEntries(2)
#legend.SetEntry(0,catsphere.GetOutput(),"maximacat",[1.0,0.0,0.0])
#legend.SetEntry(1,catsphere.GetOutput(),"minimacat",[0.0,1.0,0.0])
#legend.GetPositionCoordinate().SetCoordinateSystemToView()
#legend.GetPositionCoordinate().SetValue(0.5,.70)
#legend.GetPosition2Coordinate().SetCoordinateSystemToView()
#legend.GetPosition2Coordinate().SetValue(0.95,0.85)
#legend.SetVisibility(1)
#
#ren = vtk.vtkRenderer()
#ren.AddActor(catsphereactor)
#ren.AddActor(outlineActor)
#ren.AddActor(critsphereactor)
#ren.AddActor(tubeActor)
#ren.AddActor(imgactor)
#ren.AddActor(legend)
#
#
#ren.SetBackground(0.2, 0.2, 0.2)
#
#renWin = vtk.vtkRenderWindow()
#renWin.AddRenderer(ren)
#renWin.SetWindowName("Atoms")
#renWin.SetSize(700, 700)
#
## Set up the keyboard interface
#keyboard_interface = KeyboardInterface()
#keyboard_interface.renWin = renWin
#
#iren = vtk.vtkRenderWindowInteractor()
#iren.SetRenderWindow(renWin)
#iren.AddObserver("KeyPressEvent", keyboard_interface.keypress)
#
#
#iren.Initialize()
#iren.Start()
#
#
