#author Sajith

import ss
import numpy as np
import scipy as sp
import cv2
import matplotlib.pyplot as plt
import time
from scipy.ndimage import label

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def gauss_kern(Img):
    """ Returns a normalized 2D gauss kernel array for convolutions """
    h2,h1 = Img.shape    
    x, y = np.mgrid[0:h2, 0:h1]
    x = x-h2/2
    y = y-h1/2
#    sigma = 13.4
    sigma = 1.5
    g = np.exp( -( x**2 + y**2 ) / (2*sigma**2) );
    return g / g.sum()
    
def createSortindex(radius):
    circle = cb.createCircleEdge(radius)
    circindex = np.argwhere(circle)
    circindex[:, 0] = circindex[:, 0] - circindex[:, 0].mean()
    circindex[:, 1] = circindex[:, 1] - circindex[:, 1].mean()
    quadrindex = np.zeros((circindex.shape[0], 4))
    quadrindex[:, 0:2] = circindex
    quadrindex[:, 2] = np.angle(quadrindex[:,0]+1j*quadrindex[:,1],deg=True)
    sortindex =  quadrindex[np.argsort(quadrindex[:,2]), 0:2]
    return(sortindex)
#def weightFind(temparr):
#    temparr = weightarray[:,:,jj]
#    uniq = np.unique(temparr)
#    uniq = uniq[uniq>0]
#    
#    for ii in range(uniq.shape[0]):
#        singlereg = temparr==uniq[ii]
#        
#        if( np.sum(singlereg)>5000 or np.sum(singlereg)<150  ):
#            temparr[temparr==uniq[ii]] = 0
#       else:
#           
#           params = cb.ellipseParam(np.int64(np.argwhere(singlereg==1)))  
#            
# change the following parameters according to the data used              
#            if( params[1] >8 and params[1]<23 and params[0] <300 and params[0]>15): #phase data 1 
#            if( params[1] >8 and params[1]<23 and params[0] <300 and params[0]>20): #phase data 2
#            if( params[1] >12 and params[1]<30 and params[0] <300 and params[0]>35): #phase data 3
#            if( params[1] >5 and params[1]<19 and params[0] <150 and params[0]>15): # fluorescent data
#
#               indivarea = np.sum(np.float32(singlereg))
#                elipsarea = params[2]                
#                                                        
#                indivperim = np.sum(np.float32(singlereg - cv2.erode(np.uint8(singlereg) ,np.ones((3,3))) ))                    
#                elipsperim = params[3]
#                                                                        
#                residual = np.abs(indivarea - elipsarea ) 
#                residuarearatio = np.min([indivarea,  elipsarea] )/np.max([indivarea,  elipsarea] )
#                residuperimratio = np.min([indivperim,  elipsperim] )/np.max([indivperim,  elipsperim] )              
#                convexity = indivarea/cb.findConvexarea(singlereg)
#                weightval = 0.5*residuarearatio + 0.5*convexity
#                
#                temparr[temparr==uniq[ii]] = weightval
#    
#            else:
#               temparr[temparr==uniq[ii]] = 0
#
#    return(temparr)
    
    
def renormalize(eigh1):
    et = np.copy(eigh1)
    rval = np.arange(eigh1.min(),eigh1.max(),10)
    arr = []
    for ii in np.arange(eigh1.min(),eigh1.max(),10):
        arr.append( (eigh1<ii).sum() )
    
    a = np.array(arr)
    b = np.float64(a)
    b = b/b.max()
    rthresh = rval[np.argwhere(b>0.01)[0]]
    et[et<rthresh] = rthresh
    temarr = et-et.min()
    temarr = temarr/temarr.max()
    temarr = np.uint8(temarr*255)
    return(temarr)
    

THRESHWIN = 5  #select threshold window around the mean value of etemp    
#%%###########################################################################

orimg = cv2.imread('phasedata3.tif',-1)
start = time.time()


orimg = np.float32(orimg)
#orimg = orimg.max()-orimg # uncomment for fluorescent data
inputimage = np.copy(orimg)

startime = time.time()


#% variable thresholding 
# find the eigen value of gaussian smoothed hessian image
Img = np.copy(orimg)
gau = gauss_kern(Img)
Imgfft = np.fft.rfft2(Img)
gfft = np.fft.rfft2(gau)
fftimage = np.multiply(Imgfft, gfft)
Img_smooth =np.real(np.fft.ifftshift( np.fft.irfft2(fftimage)))
#myshow2(Img_smooth)
Iy, Ix = np.gradient(Img_smooth)
Ixy, Ixx = np.gradient(Ix)
Iyy, Iyx = np.gradient(Iy)

eigvallam = np.zeros((2,Ixx.shape[0],Ixx.shape[1] ))
trhessian = Ixx+Iyy
dethessian = Ixx*Iyy-Ixy*Ixy

eigvallam[0,:,:] = 0.5*(trhessian + np.sqrt(trhessian*trhessian - (4*dethessian) ))
eigvallam[1,:,:] = 0.5*(trhessian - np.sqrt(trhessian*trhessian - (4*dethessian) ))
eigh1 = eigvallam.min(0)
eigh2 = eigvallam.max(0)

#cb.myshow2(eigh1)
etemp = np.copy(eigh1)
etemp = etemp-etemp.min()
etemp = etemp/etemp.max()
etemp = np.uint8(etemp*255)
#%%

#find histogram peak to detect start and end intensities
histo, inten = np.histogram(etemp,range(256))

histo = np.float32(histo)
inten = np.float32(inten)

meanval = np.int(np.sum((histo/histo.sum())*range(255)))
peakval = inten[np.argwhere(histo == histo.max())[0][0]]

startinten = meanval - THRESHWIN
stopinten = meanval + THRESHWIN

LEVELS = np.int(stopinten-startinten)
labelimgarray = np.uint16( np.zeros((etemp.shape[0], etemp.shape[1], LEVELS)))

for ii in range(LEVELS):
    labelimgarray[:,:,ii], ncc = label((etemp>=(ii+startinten)),np.ones((3,3))) 
    
#weightarray = []
#for ii in range(LEVELS):
#    weightarray.append(np.float64(labelimgarray[:,:,ii]))

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#print "dtype ",labelimgarray.dtype
labelimgarray = np.uint16(labelimgarray)
flatarray = np.zeros((labelimgarray.shape[0]*labelimgarray.shape[1],labelimgarray.shape[2]),dtype=np.float32)
for ii in range(labelimgarray.shape[2]):
    flatarray[:, ii] = labelimgarray[:, :, ii].flatten()

singlearray = labelimgarray[:,:,6]
#cv2.imwrite("/home/sajith/opencv2numpy/singlearray.tiff", singlearray)
#plt.figure(),plt.imshow(labelimgarray[:,:,6]),plt.show()
#img = cv2.imread("/home/sajith/opencv2numpy/singlearray.tiff",-1)
#print np.unique(labelimgarray[:,:,5])
#print "python shape ", labelimgarray[:,:,6].shape, " max ", labelimgarray[:,:,6].max()
#print "######################################################"
#A = np.random.random((4,3))
#out = cba.test_np_mat(img)

#input parameters
inparams = np.zeros(8)
inparams[0] = orimg.shape[0]
inparams[1] = orimg.shape[1]
inparams[2] = 5000 # max object area size
inparams[3] = 150  # min object area size
inparams[4] = 35   # min major axis length
inparams[5] = 300  # max major axis length
inparams[6] = 12   # min minor axis length
inparams[7] = 30   # max minor axis length


#out = np.zeros((labelimgarray.shape[0]*labelimgarray.shape[1],labelimgarray.shape[2]))
out = ss.findStackWeightThreaded(flatarray, inparams) # multi threaded version
#out = cba.findStackWeight(flatarray, inparams) # single threaded version

#print np.unique(out)
#for ii in range(labelimgarray.shape[2]):
#    out[:, :, ii] = cba.findWeightArray(np.float32(labelimgarray[:,:,ii]))
#print "######################################################"

maximg = out.max(0)
maximgreshape = maximg.reshape(labelimgarray.shape[0],labelimgarray.shape[1])
#plt.figure(),plt.imshow(out[0,:].reshape(labelimgarray.shape[0],labelimgarray.shape[1])),plt.show()

threshmaximg = sp.ndimage.binary_fill_holes(maximgreshape>0.75)
outimg , count = label(threshmaximg,np.ones((3,3)))
print time.time()-start
plt.figure(),plt.imshow(maximgreshape>0.75),plt.show()
