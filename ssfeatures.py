# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 17:40:30 2015

@author: Sajith Kecheril Sadanandan
sajith.ks@it.uu.se
"""

import ss
import numpy as np
import scipy.spatial.distance as sdist
import matplotlib.pyplot as plt
import skimage.morphology as skmorph

def gaussianSmooth(inputimg, sigma):
    h2,h1 = inputimg.shape    
    x, y = np.mgrid[0:h2, 0:h1]
    x = x-h2/2
    y = y-h1/2
    #sigma = 1.5
    g = np.exp( -( x**2 + y**2 ) / (2*sigma**2) );
    gau =  g / g.sum()
    
    Imgfft = np.fft.rfft2(inputimg)
    gfft = np.fft.rfft2(gau)
    fftimage = np.multiply(Imgfft, gfft)
    img = np.real(np.fft.ifftshift( np.fft.irfft2(fftimage))) * sigma
    return(img)
    

def findCriticalPoints(inputimg, sigma):
# -*- coding: utf-8 -*-
    """
    findCriticalPoints(inputimg, sigma)-->(maximapts, minimapts, saddlepts, maxval, minval, saddleval)
    Created on Wed Sep 18 09:56:20 2013
    modified on Nov 05 2015
    find the critical points for the specified image and scale
    findCriticalPoints(inputimg, sigma)
    returns maxima, minima and saddle points
    """
    
#%% find the gaussian kernel for image    

    h2,h1 = inputimg.shape    
    x, y = np.mgrid[0:h2, 0:h1]
    x = x-h2/2
    y = y-h1/2
    #sigma = 1.5
    g = np.exp( -( x**2 + y**2 ) / (2*sigma**2) );
    gau =  g / g.sum()

#%% convolution of image with gaussian kernel in frequency domain

    Imgfft = np.fft.rfft2(inputimg)
    gfft = np.fft.rfft2(gau)
    fftimage = np.multiply(Imgfft, gfft)
    img =np.real(np.fft.ifftshift( np.fft.irfft2(fftimage))) * sigma

#%% shifting the matrix

    left = np.roll(img,-1,1) #shift the matrix one element left
    right = np.roll(img,1,1) #shift the matrix one element right
    up = np.roll(img,-1,0)   #shift the matrix one element up
    down = np.roll(img,1,0)  #shift the matrix one element down

    leftup = np.roll(left,-1,0)     #shift the matrix one element left and up
    leftdown = np.roll(left,1,0)    #shift the matrix one element left and down
    rightup = np.roll(right,-1,0)   #shift the matrix one element right and up
    rightdown = np.roll(right,1,0)  #shift the matrix one element right and down

#%%find the sign of the difference with center pixel

    row, col = img.shape
    signmat = np.zeros((row,col,8))
    signmat[:,:,0] = np.sign(img -right)
    signmat[:,:,1] = np.sign(img -rightup) #even
    signmat[:,:,2] = np.sign(img -up)
    signmat[:,:,3] = np.sign(img -leftup) #odd
    signmat[:,:,4] = np.sign(img -left)
    signmat[:,:,5] = np.sign(img -leftdown) #odd
    signmat[:,:,6] = np.sign(img -down)
    signmat[:,:,7] = np.sign(img -rightdown) #even

#odd find the total sign changes
    
    signodd = np.zeros((row,col,6))
    signodd[:,:,0] = signmat[:,:,0] + signmat[:,:,1] == 0
    signodd[:,:,1] = signmat[:,:,1] + signmat[:,:,2] == 0
    signodd[:,:,2] = signmat[:,:,2] + signmat[:,:,4] == 0
    signodd[:,:,3] = signmat[:,:,4] + signmat[:,:,6] == 0
    signodd[:,:,4] = signmat[:,:,6] + signmat[:,:,7] == 0
    signodd[:,:,5] = signmat[:,:,7] + signmat[:,:,0] == 0

#even find the total sign changes

    signeven = np.zeros((row,col,6))
    signeven[:,:,0] = signmat[:,:,0] + signmat[:,:,2] == 0
    signeven[:,:,1] = signmat[:,:,2] + signmat[:,:,3] == 0
    signeven[:,:,2] = signmat[:,:,3] + signmat[:,:,4] == 0
    signeven[:,:,3] = signmat[:,:,4] + signmat[:,:,5] == 0
    signeven[:,:,4] = signmat[:,:,5] + signmat[:,:,6] == 0
    signeven[:,:,5] = signmat[:,:,6] + signmat[:,:,0] == 0

#sum the 
    accumodd = np.sum(signodd,2)
    accumeven = np.sum(signeven,2)
    accumodd[range(0,row,2),:] = 0
    accumeven[range(1,row,2),:] = 0

    together = accumodd + accumeven
    together[0,:] = 2
    together[row-1,:] = 2
    together[:,0] = 2
    together[:,col-1] = 2

    saddlepts = np.argwhere(together == 4)
    extremas = np.zeros((row,col,2))
    extremas[:,:,1] = together == 0

    extremasign = signmat[:,:,0]*extremas[:,:,1]
    minimapts = np.argwhere(extremasign == -1)    
    maximapts = np.argwhere(extremasign == 1)    
    
    Iy, Ix=np.gradient(img)
    Ixy, Ixx = np.gradient(Ix)
    Iyy, Iyx = np.gradient(Iy)
    dethessian = Ixx * Iyy - Ixy * Iyx #determinant

    maxval = []
    for ii in range(maximapts.shape[0]):
        maxval.append((dethessian[maximapts[ii,0],maximapts[ii,1]]))
    maxval = np.array(maxval,'float32')
    
    minval = []
    for ii in range(minimapts.shape[0]):
        minval.append((dethessian[minimapts[ii,0],minimapts[ii,1]]))
    minval = np.array(minval,'float32')
    
    saddleval = []
    for ii in range(saddlepts.shape[0]):
        saddleval.append((dethessian[saddlepts[ii,0],saddlepts[ii,1]]))
    saddleval = np.array(saddleval,'float32')
        
    return(maximapts, minimapts, saddlepts, maxval, minval, saddleval)



def trackCriticalPoints(maxpts):
    """
    created on 19 Oct 2013. function to track critical points and return the determinant of 
    hessian maximized scale space value of the critical path usage
    maxcrtipoint = trackCriticalPoints(maxpts)
    """
    maxptslabel = np.zeros((maxpts.shape[0],5))   
    maxptslabel[:, 0:4] = maxpts 
# initialize the labels for maxima points start by adding 100 to all the scale 
# zero points then find the euclidean distance to the point nearest to it above the scale
# only annhilation catastrophes are considered, creations are ignored
    
    for jj in range(np.unique(maxpts[:,2]).shape[0] - 1):
        if (jj == 0):
            maxptslabel[maxptslabel[:, 2] == 0, 4] = np.add(100, range(maxptslabel[maxptslabel[:,2]==0,4].shape[0])) # give labels to points startin at level 0
        lower = maxptslabel[maxpts[:, 2] == jj]
        upper = maxptslabel[maxpts[:, 2] == jj + 1]
#        print lower.shape,"_____",upper.shape
        distmat = sdist.cdist(lower[:, 0:2], upper[:,0:2] , 'euclidean')     
# assign the nearest point label to the upper scale        
        for ii in range(upper.shape[0]):
            upper[ii, 4] = lower[np.argmin(distmat[:, ii]), 4]
        #refinement step to eliminate more than one point in the higher scale being assigned same label
        for ii in range(upper.shape[0]):
            if (np.sum(upper[:,4] == upper[ii,4])>1 and upper[ii,4] != 0) :
                conflictlabel = upper[ii, 4]
                conflictarr = np.argwhere(upper[:, 4] == upper[ii,4])
                upper[conflictarr, 4 ] = 0
                upper[conflictarr[np.argmin(upper[conflictarr,3])], 4] = conflictlabel
        maxptslabel[maxpts[:, 2] == jj] = lower
        maxptslabel[maxpts[:, 2] == jj + 1] = upper
    tempmax = np.zeros((0,5))
    finmax = np.zeros((0,5))   
    finmaxlabel = np.unique(maxptslabel[:, 4])
    for ii in range(finmaxlabel.shape[0]):
        if(finmaxlabel[ii] != 0):
            maxresponse = np.argmax(maxptslabel[maxptslabel[:,4] == finmaxlabel[ii],2])
            condtn1 = maxptslabel[:, 2] == maxresponse 
            condtn2 = maxptslabel[:, 4] == finmaxlabel[ii]
            tempmax = maxptslabel[condtn1 * condtn2]
        finmax = np.concatenate((finmax,tempmax),0)
    return (finmax)
##    
#%%
def findCatastrophe(inputgray, start = 1., stop = 10., scale = 18):
    '''
    findCatastrophe(inputgray, start = 1., stop = 10., scale = 18)--> catastrophe points
    cat[x,y,scale,extremahessian,saddlehessian,1(minima cat) or 3(maxima cat)]
    '''
    maxpts = minpts = saddlepts = np.zeros((0, 4))
    scaleval = np.linspace(start,stop,scale)
    for count in range(scale):    
       # maxima, minima, saddle, maxval, minval, saddleval = findCriticalPoints(inputgray, np.exp(scaleval[count]) )
        smoothimg = gaussianSmooth(inputgray, np.exp(scaleval[count]))

        maxima = ss.findMaxima(smoothimg )
        minima = ss.findMinima(smoothimg )
        saddle = ss.findSaddle(smoothimg )

        #print maxima.shape, " ", minima.shape, " ", saddle.shape

        maxval = minval = saddleval = 0
        maximascale = np.zeros((maxima.shape[0], 4))
        maximascale[:,0:2] = maxima
        maximascale[:,2] = count
        maximascale[:,3] = maxval
        
        maxpts = np.concatenate((maxpts,maximascale),0)
        
        minimascale = np.zeros((minima.shape[0], 4))
        minimascale[:,0:2] = minima
        minimascale[:,2] = count
        minimascale[:,3] = minval
        minpts = np.concatenate((minpts,minimascale),0)
        
        saddlescale = np.zeros((saddle.shape[0], 4))
        saddlescale[:,0:2] = saddle
        saddlescale[:,2] = count
        saddlescale[:,3] = saddleval
        saddlepts = np.concatenate((saddlepts,saddlescale),0)
        
    finmax = trackCriticalPoints(maxpts)
    finmin = trackCriticalPoints(minpts)
    finsaddl = trackCriticalPoints(saddlepts)
    
    finmax[:,4] = 3
    finmin[:,4] = 1
    finsaddl[:,4] = 0
    
    topextrema = np.vstack((finmax,finmin))
    topsaddl = finsaddl
    
    cat = np.zeros((1,6))
    for currscale in range(scale):
        tempsd=topsaddl[topsaddl[:,2]==currscale]
        tempex=topextrema[topextrema[:,2] ==currscale]
        
        if(tempsd.shape[0]>tempex.shape[0]):
            bigmat = tempsd
            smallmat = tempex
        else:
            bigmat = tempex
            smallmat = tempsd
            
        distmat = sdist.cdist(smallmat[:,0:2],bigmat[:,0:2],'euclidean')
        catastrophe = np.zeros((distmat.shape[0], 6))
        
        for ii in range(distmat.shape[0]):
            minloc = np.argwhere(distmat[ii,:]==distmat[ii,:].min())[0][0]
            catastrophe[ii,0:2] = 0.5*(smallmat[ii,0:2] +  bigmat[minloc,0:2])
            catastrophe[ii,2] = currscale        
            pos = np.argwhere([smallmat[ii,4] ,  bigmat[minloc,4]]==np.max([smallmat[ii,4] ,  bigmat[minloc,4]]))[0][0]  
            if(pos==0):
                catastrophe[ii,3] = smallmat[ii,3] #extrema hessian in 3 and saddle hessian in 4
                catastrophe[ii,4] = bigmat[ii,3]
            else:
                catastrophe[ii,3] = bigmat[ii,3]
                catastrophe[ii,4] = smallmat[ii,3]
        
            catastrophe[ii,5] = np.max([smallmat[ii,4] ,  bigmat[minloc,4]])
        if(catastrophe.shape[0]!=0):
            cat = np.vstack((cat,catastrophe))
    
        cat = cat[1:,:]
    return(cat)

def normalizeImage(inputgray):
    inputgray = np.float32(inputgray)
    outimg = inputgray-inputgray.min()    
    outimg = outimg/outimg.max()
    
    outimg = np.uint8(outimg*255)
    return(outimg)
    
def showCriticalPoints(inputgray, scaleval):
    
    smoothimg = gaussianSmooth(inputgray, np.exp(scaleval))
    inimg = normalizeImage(inputgray)
    
    maxima = ss.findMaxima(smoothimg )
    minima = ss.findMinima(smoothimg )
    saddle = ss.findSaddle(smoothimg )
    
    outimg = np.zeros((smoothimg.shape[0],smoothimg.shape[1],3),dtype='uint8')
    outimg[:,:,0] = inimg
    outimg[:,:,1] = inimg
    outimg[:,:,2] = inimg

    maskimg = np.zeros((smoothimg.shape[0],smoothimg.shape[1]))    
    maskimg[maxima[:,0],maxima[:,1]]=1
    maskimg = skmorph.binary_dilation(maskimg,np.ones((3,3)))
    outimg[maskimg==1,:] = [255,0,0]

    maskimg = np.zeros((smoothimg.shape[0],smoothimg.shape[1]))    
    maskimg[minima[:,0],minima[:,1]]=1
    maskimg = skmorph.binary_dilation(maskimg,np.ones((3,3)))
    outimg[maskimg==1,:] = [0,255,0]

    maskimg = np.zeros((smoothimg.shape[0],smoothimg.shape[1]))    
    maskimg[saddle[:,0],saddle[:,1]]=1
    maskimg = skmorph.binary_dilation(maskimg,np.ones((3,3)))
    outimg[maskimg==1,:] = [0,0,255]
    
    plt.imshow(outimg),plt.show()

    