// Author: Sudeep Pillai (spillai@csail.mit.edu). Edited by Sajith
// License: BSD
// Last modified: Sep 14, 2014

// Wrapper for most external modules
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <exception>
#include <thread>
#include <future>


#include <ctime>
#include <vector>
#include <cmath>
#include <algorithm>

// Opencv includes
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// np_opencv_converter
#include "np_opencv_converter.hpp"

namespace py = boost::python;
//modified indexes

cv::Mat test_np_mat(const cv::Mat_<float>& in) {
  std::cerr << "row: " << (int)in.rows << std::endl;
  std::cerr << "col: " << (int)in.cols << std::endl;
  std::cerr << "col: " << (int)in.cols << std::endl;
  std::cerr << "depth: " << (int)in.depth() << std::endl;
  
  clock_t start1 = clock();
  //std::vector<int> temparray = findHistogram();
  clock_t end1 = clock();
  //std::cout<<"time "<<((float)(end1-start1))/CLOCKS_PER_SEC<<std::endl;
  
  return in.clone();
}

cv::Mat test_with_args(const cv::Mat_<float>& in, const int& var1 = 1,
                       const double& var2 = 10.0, const std::string& name=std::string("test_name")) {
  std::cerr << "in: " << in << std::endl;
  std::cerr << "sz: " << in.size() << std::endl;
  std::cerr << "Returning transpose" << std::endl;
  return in.t();
}

class GenericWrapper {
 public: 
  GenericWrapper(const int& _var_int = 1, const float& _var_float = 1.f,
                 const double& _var_double = 1.d, const std::string& _var_string = std::string("test_string"))
      : var_int(_var_int), var_float(_var_float), var_double(_var_double), var_string(_var_string)
  {

  }

  cv::Mat process(const cv::Mat& in) {
    std::cerr << "in: " << in << std::endl;
    std::cerr << "sz: " << in.size() << std::endl;
    std::cerr << "Returning transpose" << std::endl;
    return in.t();
  }

 private:
  int var_int;
  float var_float;
  double var_double;
  std::string var_string;  
};

/***********************************************************************************************************************************************************************************/


//struct Weightfind
//{
//	cv::Mat inimg;
//	Weightfind(cv::Mat inimg): inimg(inimg){}

	std::vector<int> findHistogram(const cv::Mat_<unsigned short>& inimg){
		double min, max;
		std::vector<int> histog;

		cv::minMaxLoc(inimg, &min, &max); //opencv function to find minimum and maximum of image.
		histog.resize((int)max + 1,0);

			for(int ii=0;ii<inimg.rows;ii++){
				for(int jj=0;jj<inimg.cols;jj++){

					histog[(int)inimg.at<unsigned short>(ii,jj)] =  histog[(int)inimg.at<unsigned short>(ii,jj)]+1;
				}
			}
		return(histog);
		}

	/*********** find the moments **************************/

		float cenMoment(std::vector<cv::Point2f> position, int xorder, int yorder){

			//find x and y average
			float meanx = 0;
			float meany = 0;
			for(auto ii:position){
				meanx += ii.x;
				meany += ii.y;
			}
			meanx = meanx/position.size();
			meany = meany/position.size();

			//find xorder,yorder order moment


			float mo = 0;
			for(auto ii:position){
				mo += std::pow(ii.x - meanx, xorder)*std::pow(ii.y - meany, yorder);
			}

			return(mo);
		}

	/************find ellipse parameters such as major and minor axes *********/

	std::vector<float> ellipseParam(std::vector<cv::Point2f> position){

		//double sum = std::ac
		/*for(auto ii:position){
			std::cout << ii.x<<std::endl;
		}*/
		//cv::Moments allmoments = cv::moments(position,false);
		std::vector<float> param;

		// find central moments from coordinates
		float m00 = cenMoment(position, 0, 0);
		float m11 = cenMoment(position, 1, 1);
		float m20 = cenMoment(position, 2, 0);
		float m02 = cenMoment(position, 0, 2);


		//Eigen values found using this method. http://www.math.harvard.edu/archive/21b_fall_04/exhibits/2dmatrices/index.html

		// using trace and determinant from moment matrix, ([[m02,m11],[m11,m20]])

		float trace = m02 + m20;
		float det = (m02 * m20) - (m11 * m11);

		//std::cout<<"m02 "<<allmoments.m02<<" m00 "<< allmoments.m00<< std::cout<<std::endl;

		float lambda1 = trace/2 + std::pow( ( (std::pow(trace, 2)/4) - det), 0.5);
		float lambda2 = trace/2 - std::pow( ( (std::pow(trace, 2)/4) - det), 0.5);

		float maj = 4*pow(lambda1/m00,0.5);
		float min = 4*pow(lambda2/m00,0.5);

		float area = M_PI * (maj/2)*(min/2);

		param.push_back(maj);
		param.push_back(min);
		param.push_back(area);

		return(param);
		}

	/************ find convex area ****************************/

		float findConvexarea(std::vector<cv::Point2f> position){

			std::vector<cv::Point2f> hull;
			//cv::findContours()
			cv::convexHull(position,hull);
			float area = cv::contourArea(hull,false);

			return(area);
		}


	/**************** find minimum and maximum *******/
	unsigned short findMin(cv::Mat inimg){
		unsigned short intensityval;
		unsigned short min = 100;
		//std::cout<<" rows "<<(int)inimg.rows<< " cols " <<inimg.cols <<std::endl;
		for(int ii=0;ii<inimg.rows;ii++){
			for(int jj=0;jj<inimg.cols;jj++){

				intensityval = (unsigned short)inimg.at<unsigned short>(ii,jj);

				//std::cout<<" min val ii --- "<<intensityval<<std::endl;

				if(intensityval < min){
					min = (unsigned short)intensityval;
				}
				}
			}
		//std::cout<<" min val --- "<<min<<std::endl;
		return(min);
	}

	unsigned short findMax(cv::Mat inimg){
		unsigned short intensityval;
		unsigned short max = 0;
		for(int ii=0;ii<inimg.rows;ii++){
			for(int jj=0;jj<inimg.cols;jj++){

				intensityval = (unsigned short)inimg.at<unsigned short>(ii,jj);
				if((unsigned short)intensityval>max){
					max = (unsigned short)intensityval;
				}
				}
			}
		//std::cout<<" max val --- "<<max<<std::endl;
		return(max);
	}
	/****************find location of each label values******/
	std::vector<std::vector<cv::Point2f>>  findposition(const cv::Mat_<unsigned short>& inimg){
		unsigned short min, max;


		//std::cout<<" before "<<std::endl;
		//cv::minMaxLoc(inimg, &min, &max); //opencv function to find minimum and maximum of image.
		min = findMin(inimg);
		max = findMax(inimg);

		//std::cout<<" after " << min << " " <<max<<std::endl;


		int intensityval;
		cv::Point2f temploc;								//single location of positions;
		std::vector<std::vector<cv::Point2f>> allpos(max+1);

		//std::vector<std::vector<cv::Point2i>> contours;
		//std::vector<Vec4i> hierarchy;

		for(int ii=0;ii<inimg.rows;ii++){
			for(int jj=0;jj<inimg.cols;jj++){

				intensityval = (int)inimg.at<unsigned short>(ii,jj);
				//intensityval = (int)inimg.at<unsigned short>(jj,ii);
				temploc.y = (float)ii;
				temploc.x = (float)jj;
				allpos[intensityval].push_back(temploc);

				}
			}

		//std::cout<<"size "<<allpos[1].size()<<std::endl;

		/*for(auto ii:allpos){
			if(ii.size())
			std::vector<float> ellipsepar = ellipseParam(ii);
			//std::cout<<" major axis "<< ellipsepar[0] << " minor axis "<< ellipsepar[1]<<std::endl;

			float temp = findConvexarea(ii);

		}*/

		//findWeightArray(allpos);
		return(allpos);
		}





	/*********** find the weights *******************************/

	cv::Mat findWeightArray(const cv::Mat_<unsigned short>& inimg, cv::Mat inparams){
		//std::cout<<" =====rows =====  "<<inimg.rows <<std::endl;
		cv::Mat copyimg;
		inimg.copyTo(copyimg);

		std::vector<std::vector<cv::Point2f>> allpos = findposition( copyimg);

		cv::Mat weightarray = cv::Mat(inimg.rows, inimg.cols, CV_32FC1, cvScalar(0.0));
		//std::cout<<" ====!!!!!!!!!!!!==========  " <<std::endl;

		// Area based filtering
		//std::cout<<" (int)allpos.size() "<<(int)allpos.size() <<std::endl;

		std::vector<float> weightval((int)allpos.size(), 0.0);
		//std::cout<<" ==== size "<<(int)allpos.size() <<std::endl;
		//std::cout<<" after2 "<<std::endl;

		int counter = 0;
		for(auto indivreg:allpos){

			std::vector<cv::Point2f> temp = indivreg;
			// std::cout<<" counter "<< counter<<std::endl;

			if(temp.size() < inparams.at<float>(2) && temp.size() > inparams.at<float>(3) && counter != 0){ //area to be fed as parameter

				std::vector<float> params = ellipseParam(temp);

				if( params[0] > inparams.at<float>(4) && params[0] < inparams.at<float>(5) && params[1] > inparams.at<float>(6) && params[1] < inparams.at<float>(7) ){ // major and minor axes to be fed as parameters

					float residualarearatio = std::min((float)temp.size(), (float)params[2])/std::max((float)temp.size(), (float)params[2]); // ratio of individual area and ellipse area

					float convexity = std::min((float)temp.size(), (float)findConvexarea(temp))/std::max((float)temp.size(), (float)findConvexarea(temp)); // ratio of individual area and convex area

					weightval[counter] = 0.5*residualarearatio + 0.5*convexity;


				}
			}

			counter++;

		}

		counter = 0;
		for(auto indivweight:weightval){
		//for(int ii=0;ii<(int)weightval.size();ii++){
			if(indivweight > 0.0){
				for(auto pos:allpos[counter]){
					weightarray.at<float>(pos.y,pos.x) = indivweight;
				}
			}

			counter++;
		}
		//cv::imwrite("outimg.tiff",weightarray*255);
		return(weightarray);
	}



//};

/*********** reshape image (flattened image to original image)*******************************/

	cv::Mat reshapeImage(cv::Mat inputimg, int row, int col){

		cv::Mat firstarray = cv::Mat(row, col, CV_32FC1, cvScalar(0.0));
		int count = 0;
		for(int ii = 0; ii<row; ii++){
			for(int jj = 0; jj<col; jj++){
				firstarray.at<float>(ii, jj) = inputimg.at<float>(count,0);
				count++;
			}
		}
		return(firstarray);
	}

/*********** reshape image (flattened image to original image)*******************************/

	cv::Mat flattenImage(cv::Mat inputimg, long long int rowval){

		cv::Mat firstarray = cv::Mat(1, rowval, CV_32FC1, cvScalar(0.0));
		long long int count = 0;
		for(int ii = 0; ii<inputimg.rows; ii++){
			for(int jj = 0; jj<inputimg.cols; jj++){
				firstarray.at<float>(0, count) = (float)inputimg.at<float>(ii, jj);
				//std::cout << firstarray.at<float>(count, 0) << std::endl;
				count++;
			}
		}
		return(firstarray);
	}

/*********** find label stack weights *******************************/

	cv::Mat findStackWeight(const cv::Mat_<float>& flatarray, const cv::Mat_<float>& inparams){
		//std::cout <<" stack flatarray rows " << flatarray.rows <<" cols " << flatarray.cols<< std::endl;
		cv::Mat outarray =  cv::Mat(0, flatarray.cols, CV_32FC1, cvScalar(0.0));

		for (int ii=0;ii<flatarray.cols;ii++){
		//for (int ii=0;ii<1;ii++){
			cv::Mat inimg = cv::Mat(inparams.at<float>(0), inparams.at<float>(1), CV_32FC1, cvScalar(0.0));
			cv::Mat singleslice = flatarray.colRange(ii,ii+1);

			inimg = reshapeImage(singleslice, inparams.at<float>(0), inparams.at<float>(1));

			//std::cout <<" stack rows " << inimg.rows <<" cols " << inimg.cols<< std::endl;
			//return(labelimgarray.dims)
			cv::Mat weightarray = findWeightArray(inimg, inparams);

			//std::cout <<" single val " << weightarray.at<float>(350,350) << std::endl;

			outarray.push_back(flattenImage(weightarray, inparams.at<float>(0)*inparams.at<float>(1)));// treating Mat as vectors, src http://stackoverflow.com/questions/6994171/copy-select-rows-into-new-matrix

			//std::cout <<" stack val " << (float)outarray.at<float>(302211,ii) <<" cols " << outarray.cols<< std::endl;

		}

		//std::cout<<" outarray.rows " << outarray.rows<< std::endl;
		//std::cout<<" outarray.cols "<<outarray.cols<< std::endl;

		//std::cout<<" type "<<sizeof(float)<< std::endl;

		return(outarray);
	}

/*********** find label stack weights multi threaded *******************************/

/*********** process flat array *******************************/
	cv::Mat processFlatArray(cv::Mat singleflatarray, cv::Mat inparams){

		cv::Mat inimg = cv::Mat(inparams.at<float>(0), inparams.at<float>(1), CV_32FC1, cvScalar(0.0));
		inimg = reshapeImage(singleflatarray, inparams.at<float>(0), inparams.at<float>(1));
		//std::cout<<" outarray.rows " << std::endl;
		cv::Mat weightarray = findWeightArray(inimg, inparams);
		//std::cout<<"after findweightarray " << std::endl;
		cv::Mat flatimage = flattenImage(weightarray, inparams.at<float>(0)*inparams.at<float>(1));

		return(flatimage);
	}

	cv::Mat findStackWeightThreaded(const cv::Mat_<float>& flatarray, const cv::Mat_<float>& inparams){
		/*
		 * 	inparams[0] = orimg.shape[0]
			inparams[1] = orimg.shape[1]
			inparams[2] = 5000 # max object area size
			inparams[3] = 150  # min object area size
			inparams[4] = 45   # min major axis length
			inparams[5] = 300  # max major axis length
			inparams[6] = 12   # min minor axis length
			inparams[7] = 30   # max minor axis length
		 * 
		 * 
		 * */
		
		 
		cv::Mat outarray =  cv::Mat(0, flatarray.cols, CV_32FC1, cvScalar(0.0));
		//std::cout<<"params 0 "<<inparams.at<float>(7)<<std::endl;
		
		int ii =0;
//for (int ii=0;ii<flatarray.cols;ii=ii+5){

			cv::Mat singleflatarray1 = flatarray.colRange(ii,ii+1);
			cv::Mat singleflatarray2 = flatarray.colRange(ii+1,ii+2);
			cv::Mat singleflatarray3 = flatarray.colRange(ii+2,ii+3);
			cv::Mat singleflatarray4 = flatarray.colRange(ii+3,ii+4);
			cv::Mat singleflatarray5 = flatarray.colRange(ii+4,ii+5);
			cv::Mat singleflatarray6 = flatarray.colRange(ii+5,ii+6);
			cv::Mat singleflatarray7 = flatarray.colRange(ii+6,ii+7);
			cv::Mat singleflatarray8 = flatarray.colRange(ii+7,ii+8);
			cv::Mat singleflatarray9 = flatarray.colRange(ii+8,ii+9);
			cv::Mat singleflatarray10 = flatarray.colRange(ii+9,ii+10);

			// create promises
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task1(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task2(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task3(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task4(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task5(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task6(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task7(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task8(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task9(processFlatArray);
			std::packaged_task<cv::Mat(cv::Mat, cv::Mat)> task10(processFlatArray);


			//std::cout << "promises created" <<std::endl;
			// get futures
			auto future1 = task1.get_future();
			auto future2 = task2.get_future();
			auto future3 = task3.get_future();
			auto future4 = task4.get_future();
			auto future5 = task5.get_future();
			auto future6 = task6.get_future();
			auto future7 = task7.get_future();
			auto future8 = task8.get_future();
			auto future9 = task9.get_future();
			auto future10 = task10.get_future();

			//std::cout << "futures created" <<std::endl;

			// schedule futures
			std::thread t1(move(task1),singleflatarray1, inparams);
			std::thread t2(move(task2),singleflatarray2, inparams);
			std::thread t3(move(task3),singleflatarray3, inparams);
			std::thread t4(move(task4),singleflatarray4, inparams);
			std::thread t5(move(task5),singleflatarray5, inparams);
			std::thread t6(move(task6),singleflatarray6, inparams);
			std::thread t7(move(task7),singleflatarray7, inparams);
			std::thread t8(move(task8),singleflatarray8, inparams);
			std::thread t9(move(task9),singleflatarray9, inparams);
			std::thread t10(move(task10),singleflatarray10, inparams);

			//std::cout << "threads created" <<std::endl;

			t1.join();
			t2.join();
			t3.join();
			t4.join();
			t5.join();
			t6.join();
			t7.join();
			t8.join();
			t9.join();
			t10.join();

			//std::cout << "threads joined" <<std::endl;
		//for (int ii=0;ii<2;ii++){


			//auto future1 = std::async(processFlatArray, singleflatarray1);
			//auto future2 = std::async(processFlatArray, singleflatarray2);

			//cv::Mat flatimage = processFlatArray( singleflatarray);

			//std::cout <<" single val " << weightarray.at<float>(350,350) << std::endl;

			outarray.push_back(future1.get());// treating Mat as vectors, src http://stackoverflow.com/questions/6994171/copy-select-rows-into-new-matrix
			outarray.push_back(future2.get());
			outarray.push_back(future3.get());
			outarray.push_back(future4.get());
			outarray.push_back(future5.get());
			outarray.push_back(future6.get());
			outarray.push_back(future7.get());
			outarray.push_back(future8.get());
			outarray.push_back(future9.get());
			outarray.push_back(future10.get());

			//std::cout <<" stack val " << (float)outarray.at<float>(302211,ii) <<" cols " << outarray.cols<< std::endl;

		//}
		return(outarray);
	}



/***********************************************************************************************************************************************************************************/


// Wrap a few functions and classes for testing purposes
namespace fs { namespace python {

BOOST_PYTHON_MODULE(cba)
{
  // Main types export
  fs::python::init_and_export_converters();
  py::scope scope = py::scope();

  boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
  // Basic test
  py::def("test_np_mat", &test_np_mat);

  //single array weight
  py::def("findWeightArray", &findWeightArray);
  // full stack weight
  py::def("findStackWeight",&findStackWeight);

  // multi threaded version of findStackWeight
  py::def("findStackWeightThreaded", &findStackWeightThreaded);

  // With arguments
  py::def("test_with_args", &test_with_args,
          (py::arg("src"), py::arg("var1")=1, py::arg("var2")=10.0, py::arg("name")="test_name"));

  // Class
  py::class_<GenericWrapper>("GenericWrapper")
      .def(py::init<py::optional<int, float, double, std::string> >(
          (py::arg("var_int")=1, py::arg("var_float")=1.f, py::arg("var_double")=1.d,
           py::arg("var_string")=std::string("test"))))
      .def("process", &GenericWrapper::process)
      ;


}

} // namespace fs
} // namespace python



